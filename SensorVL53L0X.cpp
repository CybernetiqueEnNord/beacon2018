#include "SensorVL53L0X.h"

SensorVL53L0XConfig::SensorVL53L0XConfig() {
}

SensorVL53L0XConfig::SensorVL53L0XConfig(uint8_t address, long degrees) : address(address), angle(toAngle(degrees)) {
}

void SensorVL53L0XConfig::setAngle(long degrees) {
  angle = toAngle(degrees);
}

uint8_t SensorVL53L0XConfig::toAngle(long degrees) {
  return (degrees * 256) / 360;
}

long SensorVL53L0XConfig::getAngle() const {
  return ((long)angle * 360) / 256;
}

void SensorVL53L0XConfig::clear() {
  address = 0;
  angle = 0;
}

SensorVL53L0X::SensorVL53L0X() {
}

const SensorVL53L0XConfig& SensorVL53L0X::getConfig() const {
  return config;
}

void SensorVL53L0X::setConfig(SensorVL53L0XConfig &config) {
  this->config = config;
  enabled = true;
}

void SensorVL53L0X::setEnabled(bool value) {
  enabled = value;
}

bool SensorVL53L0X::isEnabled() const {
  return enabled && isConfigured();
}

bool SensorVL53L0X::isConfigured() const {
  return config.address != 0;
}

void SensorVL53L0X::clearConfiguration() {
  config.clear();
  enabled = true;
}

void SensorVL53L0X::setDistance(unsigned int value) {
  measure.timestamp = millis();
  measure.distance = value;
}

const SensorMeasure& SensorVL53L0X::getMeasure() const {
  return measure;
}

