// Configuration

#define SDA_PORT PORTC
#define SDA_PIN 0
#define SCL_PORT PORTC
#define SCL_PIN 1

#define I2C_TIMEOUT 5
#define I2C_FASTMODE 1

//

#include <EEPROMex.h>
#include <Wire.h>
extern "C" {
#include <utility/twi.h>
}

#include "SoftWire.h"
#include "SensorVL53L0X.h"

#define PIN_BUZZER 13
#define PIN_START_LED 11
#define PIN_START_IN 10
#define PIN_SENSOR_LOW 2
#define PIN_SENSOR_HIGH 9

#define PIN_ANALOG_1 2
#define PIN_ANALOG_2 3

#define EEPROM_I2C_ADDRESS 0
#define EEPROM_DISTANCE_THRESHOLD (EEPROM_I2C_ADDRESS + sizeof(uint8_t))
#define EEPROM_SENSORS_COUNT (EEPROM_DISTANCE_THRESHOLD + sizeof(unsigned int))
#define EEPROM_SENSORS_CONFIG (EEPROM_SENSORS_COUNT + sizeof(uint8_t))

#define COMMAND_SERIAL_I2C_ADDRESS 'a'
#define COMMAND_SERIAL_I2C_SCAN 's'
#define COMMAND_SERIAL_EXIT 'x'
#define COMMAND_SERIAL_SENSOR_ADD 'i'
#define COMMAND_SERIAL_SENSOR_CHANGE_ADDRESS 'c'
#define COMMAND_SERIAL_SENSOR_LIST 'l'
#define COMMAND_SERIAL_SENSOR_REMOVE 'r'
#define COMMAND_SERIAL_SENSOR_PING 'p'
#define COMMAND_SERIAL_DISTANCE_THRESHOLD 't'

#define COMMAND_I2C_STATUS 's'
#define COMMAND_I2C_MEASURES 'm'
#define COMMAND_I2C_HMI 'h'
#define COMMAND_I2C_VERSION 'v'
#define COMMAND_I2C_THRESHOLD 't'
#define COMMAND_I2C_CONFIG 'c'

#define ARRAY_SIZE(x) (sizeof x / sizeof(x[0]))

#define DELAY_SHUTDOWN 1

#define TIMEOUT_MEASURE 500

#define DEFAULT_DISTANCE_THRESHOLD 1500

enum class State {
  Init,
  Start,
  Main,
  Debug
};

enum class Obstacle {
  None = 0,
  Front = 1,
  Back = 2
};

inline Obstacle operator&(Obstacle a, Obstacle b) {
  return static_cast<Obstacle>(static_cast<int>(a) & static_cast<int>(b));
}

inline Obstacle operator|=(Obstacle a, Obstacle b) {
  return static_cast<Obstacle>(static_cast<int>(a) | static_cast<int>(b));
}

PROGMEM const char * const versionString = "vBeacon " __DATE__ " - " __TIME__;

struct BeaconConfig {
  uint8_t slaveAddress = 0xFF;
  unsigned int distanceThreshold = DEFAULT_DISTANCE_THRESHOLD;
  SensorVL53L0X sensors[6];
};

struct HMIData {
  uint8_t value1;
  uint8_t value2;
};

Obstacle obstacle = Obstacle::None;
bool started = false;

BeaconConfig beaconConfig;
HMIData hmi;
State state;
SoftWire master;
#define slave Wire

void printVersion() {
  Serial.println(versionString);
}

void printHex(uint8_t value, bool addNewLine = true) {
  if (value < 16) {
    Serial.print('0');
  }
  Serial.print(value, HEX);
  if (addNewLine) {
    Serial.println();
  }
}

void printSlaveAddress() {
  Serial.print('A');
  printHex(beaconConfig.slaveAddress);
}

void printDistanceThreshold() {
  Serial.print('T');
  Serial.println(beaconConfig.distanceThreshold);
}

void printConfig() {
  printSlaveAddress();
  printDistanceThreshold();
}

void printError() {
  Serial.println(F("ERROR"));
}

void printSuccess() {
  Serial.println(F("OK"));
}

void printObstacle() {
  if (obstacle != Obstacle::None) {
    Serial.print(F("OBSTACLE"));
  }
  if ((obstacle & Obstacle::Front) != Obstacle::None) {
    Serial.print(F(" FRONT"));
  }
  if ((obstacle & Obstacle::Back) != Obstacle::None) {
    Serial.print(F(" BACK"));
  }
  Serial.println();
}

void printSensor(uint8_t index) {
  const SensorVL53L0X &sensor = beaconConfig.sensors[index];
  const SensorVL53L0XConfig &config = sensor.getConfig();
  Serial.print('S');
  Serial.print(index);
  Serial.print(';');
  printHex(config.address, false);
  Serial.print(';');
  Serial.println(config.getAngle());
}

void printSensorMeasure(uint8_t index) {
  const SensorVL53L0X &sensor = beaconConfig.sensors[index];
  const SensorMeasure &measure = sensor.getMeasure();
  Serial.print('M');
  Serial.print(index);
  Serial.print(';');
  Serial.print(measure.distance);
  Serial.print(';');
  Serial.println(sensor.isEnabled() ? '1' : '0');
}

void printSensorsList() {
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    if (beaconConfig.sensors[i].isConfigured()) {
      printSensor(i);
    } else {
      break;
    }
  }
  printSuccess();
}

void printSensorsMeasures() {
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    if (beaconConfig.sensors[i].isConfigured()) {
      printSensorMeasure(i);
    } else {
      break;
    }
  }
  printSuccess();
}

void setSlaveAddress(uint8_t address) {
  beaconConfig.slaveAddress = address;
  twi_setAddress(address);
  printSlaveAddress();
}

void addSensor(uint8_t address, long angle) {
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    if (!beaconConfig.sensors[i].isConfigured()) {
      SensorVL53L0XConfig config(address, angle);
      beaconConfig.sensors[i].setConfig(config);
      printSuccess();
      return;
    }
  }
  // Tous les capteurs ont déjà été définis
  printError();
}

void removeSensor(uint8_t index) {
  const int n = ARRAY_SIZE(beaconConfig.sensors);
  if (index >= n) {
    printError();
  }
  while (index < n - 1) {
    beaconConfig.sensors[index] = beaconConfig.sensors[index + 1];
    index++;
  }
  beaconConfig.sensors[n - 1].clearConfiguration();
  printSuccess();
}

void scanMasterBus() {
  Serial.println(F(":scanning I2C bus"));
  for (uint8_t address = 1; address < 127; address++) {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    master.beginTransmission(address);
    uint8_t error = master.endTransmission();

    if (error == 0) {
      Serial.print('D');
      printHex(address);
    }
  }
  printSuccess();
}

bool setSensorAddress(uint8_t index, uint8_t newAddress) {
  Serial.print(F(":setting sensor address at index "));
  Serial.print(index);
  Serial.print(F(" to "));
  printHex(newAddress);

  index += PIN_SENSOR_LOW;

  // reset
  setSensorShutdown(index, true);
  delay(DELAY_SHUTDOWN);
  setSensorShutdown(index, false);
  delay(DELAY_SHUTDOWN);

  VL53L0X<SoftWire> sensor(master);
  sensor.setAddress(newAddress);
  return sensor.last_status == 0;
}

bool isSensorPresent(int8_t address) {
  master.beginTransmission(address);
  uint8_t status = master.endTransmission();
  return status == 0;
}

void setDistanceThreshold(unsigned int distance) {
  if (distance == 0) {
    printError();
    return;
  }
  beaconConfig.distanceThreshold = distance;
  printDistanceThreshold();
}

void parseInput(const char *buffer, int length) {
  switch (buffer[0]) {
    case COMMAND_SERIAL_I2C_ADDRESS:
      {
        uint8_t address = strtol(&buffer[1], NULL, 16);
        if (address == 0) {
          goto error;
        }
        setSlaveAddress(address);
        break;
      }

    case COMMAND_SERIAL_I2C_SCAN:
      scanMasterBus();
      break;

    case COMMAND_SERIAL_SENSOR_ADD:
      {
        char *buf;
        uint8_t address = strtol(&buffer[1], &buf, 16);
        if (address == 0 || *buf != ';') {
          goto error;
        }
        long angle = strtol(buf + 1, &buf, 10);
        addSensor(address, angle);
        break;
      }

    case COMMAND_SERIAL_SENSOR_REMOVE:
      {
        uint8_t index = atoi(&buffer[1]);
        removeSensor(index);
        break;
      }

    case COMMAND_SERIAL_SENSOR_LIST:
      printSensorsList();
      break;

    case COMMAND_SERIAL_SENSOR_CHANGE_ADDRESS:
      {
        char *buf;
        uint8_t index = strtol(&buffer[1], &buf, 10);
        if (index < 0 || index >= ARRAY_SIZE(beaconConfig.sensors) || *buf != ';') {
          goto error;
        }
        uint8_t newAddress = strtol(buf + 1, &buf, 16);
        if (newAddress == 0) {
          goto error;
        }
        if (!setSensorAddress(index, newAddress)) {
          goto error;
        }
        break;
      }

    case COMMAND_SERIAL_SENSOR_PING:
      {
        uint8_t address = strtol(&buffer[1], NULL, 16);
        if (address == 0 || !isSensorPresent(address)) {
          goto error;
        }
        printSuccess();
        break;
      }

    case COMMAND_SERIAL_DISTANCE_THRESHOLD:
      {
        unsigned int distance = atoi(&buffer[1]);
        setDistanceThreshold(distance);
        break;
      }

    case COMMAND_SERIAL_EXIT:
      exitDebug();
      break;

    default:
      goto error;
      break;
  }
  return;

error:
  printError();
}

void handleInput() {
  char buffer[10];
  int n = Serial.readBytesUntil('\n', buffer, ARRAY_SIZE(buffer) - 1);
  if (n > 0) {
    buffer[n] = 0;
    parseInput(buffer, n);
  }
}

void runDebug() {
  if (Serial.available()) {
    handleInput();
  }

  static unsigned long last = 0;
  unsigned long now = millis();
  if (now - last > 250) {
    last = now;
    readSensors();
    checkObstacle();
    printSensorsMeasures();
  }
}

void initializeSensor(uint8_t index, SensorVL53L0X &sensor) {
  if (!sensor.isConfigured()) {
    return;
  }

  const SensorVL53L0XConfig &config = sensor.getConfig();
  if (!setSensorAddress(index, config.address)) {
    sensor.setEnabled(false);
    return;
  }

  VL53L0X<SoftWire> device(master, config.address);
  if (!device.init() || device.last_status != 0) {
    goto error;
  }
  device.setTimeout(500);
  if (!device.setVcselPulsePeriod(vcselPeriodType::VcselPeriodPreRange, 18) || device.last_status != 0) {
    goto error;
  }
  if (!device.setVcselPulsePeriod(vcselPeriodType::VcselPeriodFinalRange, 14) || device.last_status != 0) {
    goto error;
  }
  if (!device.setMeasurementTimingBudget(66000) || device.last_status != 0) {
    goto error;
  }

  // Start continuous back-to-back mode (take readings as
  // fast as possible).  To use continuous timed mode
  // instead, provide a desired inter-measurement period in
  // ms (e.g. sensor.startContinuous(100)).
  device.startContinuous();
  if (device.last_status != 0) {
    goto error;
  }
  return;

error:
  printError();
  sensor.setEnabled(false);
}

void setSensorShutdown(uint8_t index, bool value) {
  if (value) {
    // extinction : mise à 0V
    setPinModeAndValue(index, OUTPUT, LOW);
  } else {
    // allumage : utilisation des pull-ups
    setPinModeAndValue(index, INPUT, LOW);
  }
}

void shutdownSensors() {
  for (uint8_t i = PIN_SENSOR_LOW; i <= PIN_SENSOR_HIGH; i++) {
    setPinModeAndValue(i, OUTPUT, LOW);
  }
}

void initializeSensors() {
  obstacle = Obstacle::None;
  shutdownSensors();
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    SensorVL53L0X &sensor = beaconConfig.sensors[i];
    initializeSensor(i, sensor);
  }
}

void initializeMasterI2C() {
  master.begin();
}

void initializeSlaveI2C() {
  slave.begin(beaconConfig.slaveAddress);
  slave.onRequest(i2cHandleSlaveRequest);
  slave.onReceive(i2cHandleSlaveInput);
}

void i2cSendStatus() {
  uint8_t status = static_cast<int>(obstacle) << 0;
  status |= (started ? 1 : 0) << 2;
  slave.write(status);
}

void updateHMIState() {
  hmi.value1 = analogRead(PIN_ANALOG_1) >> 2;
  hmi.value2 = analogRead(PIN_ANALOG_2) >> 2;
}

void i2cSendHMIState() {
  updateHMIState();
  slave.write(hmi.value1);
  slave.write(hmi.value2);
}

void i2cSendSensorConfiguration() {
  unsigned long now = millis();
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    const SensorVL53L0X &sensor = beaconConfig.sensors[i];
    if (!sensor.isConfigured()) {
      continue;
    }
    const SensorVL53L0XConfig &config = sensor.getConfig();
    slave.write(i);
    slave.write(config.angle);
  }
  slave.write(0xFF);
}

void i2cSendSensorMeasures() {
  unsigned long now = millis();
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    const SensorVL53L0X &sensor = beaconConfig.sensors[i];
    if (!sensor.isEnabled()) {
      continue;
    }
    const SensorMeasure &measure = sensor.getMeasure();
    if (now - measure.timestamp < TIMEOUT_MEASURE) {
      slave.write(i);
      slave.write(measure.distance);
    }
  }
  slave.write(0xFF);
}

void i2cSendVersion() {
  slave.write(versionString);
}

void i2cSetThreshold() {
  if (slave.available() >= sizeof(uint8_t)) {
    beaconConfig.distanceThreshold = slave.read() * 8;
  }
}

void i2cHandleCommand(char command) {
  switch (command) {
    case COMMAND_I2C_STATUS:
      i2cSendStatus();
      break;

    case COMMAND_I2C_CONFIG:
      i2cSendSensorConfiguration();
      break;

    case COMMAND_I2C_MEASURES:
      i2cSendSensorMeasures();
      break;

    case COMMAND_I2C_HMI:
      i2cSendHMIState();
      break;

    case COMMAND_I2C_VERSION:
      i2cSendVersion();
      break;

    case COMMAND_I2C_THRESHOLD:
      i2cSetThreshold();
      break;
  }
}

void i2cHandleSlaveInput(int bytesCount) {
  while (slave.available() > 0) {
    char c = slave.read();
    i2cHandleCommand(c);
  }
}

void i2cHandleSlaveRequest() {
  i2cSendStatus();
}

void runStart() {
  state = State::Main;
  printConfig();
  initializeSensors();
  initializeSlaveI2C();
}

void runInitialization() {
  state = State::Start;
  loadConfig();
  initializeMasterI2C();
}

void enterDebug() {
  state = State::Debug;
  Serial.println(F(":entering debug mode"));
  setObstacle(Obstacle::None);
  printSensorsList();
}

void exitDebug() {
  saveConfig();
  state = State::Start;
  Serial.println(F(":exiting debug mode"));
}

void readSensor(SensorVL53L0X &sensor) {
  if (!sensor.isEnabled()) {
    return;
  }

  const SensorVL53L0XConfig &config = sensor.getConfig();
  VL53L0X<SoftWire> device(master, config.address);
  device.setTimeout(1);
  unsigned int range = device.readRangeContinuousMillimeters();
  if (!device.timeoutOccurred()) {
    sensor.setDistance(range);
  }
}

void readSensors() {
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    readSensor(beaconConfig.sensors[i]);
  }
}

void setObstacle(Obstacle value) {
  bool changed = value != obstacle;
  if (!changed) {
    return;
  }
  obstacle = value;
  printObstacle();
  bool b = (obstacle != Obstacle::None) && (state != State::Debug);
  digitalWrite(PIN_BUZZER, b);
}

void checkObstacle() {
  Obstacle obstacle = Obstacle::None;
  unsigned long now = millis();
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    const SensorVL53L0X &sensor = beaconConfig.sensors[i];
    if (!sensor.isEnabled()) {
      continue;
    }
    const SensorMeasure &measure = sensor.getMeasure();
    if (now - measure.timestamp < TIMEOUT_MEASURE) {
      bool b = measure.distance > 0 && measure.distance < beaconConfig.distanceThreshold;
      if (!b) {
        continue;
      }
      const SensorVL53L0XConfig &config = sensor.getConfig();
      long angle = config.getAngle();
      if (angle < -180 || angle > 180) {
        obstacle |= Obstacle::Back;
      } else {
        obstacle |= Obstacle::Front;
      }
    }
  }
  setObstacle(obstacle);
}

void runMain() {
  if (Serial.available()) {
    if (Serial.find("d\n")) {
      enterDebug();
      return;
    }
  }
  readSensors();
  checkObstacle();
}

void loadConfig() {
  beaconConfig.slaveAddress = EEPROM.read(EEPROM_I2C_ADDRESS);
  unsigned int distanceThreshold = EEPROM.readInt(EEPROM_DISTANCE_THRESHOLD);
  if (distanceThreshold == 0xFFFF) {
    distanceThreshold = DEFAULT_DISTANCE_THRESHOLD;
  }
  beaconConfig.distanceThreshold = distanceThreshold;
  uint8_t count = EEPROM.read(EEPROM_SENSORS_COUNT);
  if (count == 0xFF) {
    // Pas de données
    return;
  }
  uint8_t max = ARRAY_SIZE(beaconConfig.sensors);
  if (count > max) {
    count = max;
  }
  int address = EEPROM_SENSORS_CONFIG;
  for (int i = 0; i < count; i++) {
    SensorVL53L0XConfig config;
    address += EEPROM.readBlock(address, config);
    beaconConfig.sensors[i].setConfig(config);
  }
}

void saveConfig() {
  EEPROM.write(EEPROM_I2C_ADDRESS, beaconConfig.slaveAddress);
  EEPROM.writeInt(EEPROM_DISTANCE_THRESHOLD, beaconConfig.distanceThreshold);
  uint8_t count = 0;
  int address = EEPROM_SENSORS_CONFIG;
  for (uint8_t i = 0; i < ARRAY_SIZE(beaconConfig.sensors); i++) {
    if (!beaconConfig.sensors[i].isConfigured()) {
      continue;
    }
    const SensorVL53L0XConfig &config = beaconConfig.sensors[i].getConfig();
    address += EEPROM.writeBlock(address, config);
    count++;
  }
  EEPROM.write(EEPROM_SENSORS_COUNT, count);
}

void setPinModeAndValue(uint8_t index, uint8_t mode, uint8_t value) {
  digitalWrite(index, value);
  pinMode(index, mode);
}

void setupPins() {
  setPinModeAndValue(PIN_START_IN, INPUT, HIGH);
  setPinModeAndValue(PIN_START_LED, OUTPUT, LOW);
  setPinModeAndValue(PIN_BUZZER, OUTPUT, LOW);

  shutdownSensors();

  // active le capteur d'indice 0
  digitalWrite(PIN_SENSOR_LOW, HIGH);
}

void setup() {
  setupPins();

  Serial.begin(115200);
  Serial.setTimeout(3);
  printVersion();
}

void loop() {
  switch (state) {
    case State::Init:
      runInitialization();
      break;

    case State::Start:
      runStart();
      break;

    case State::Main:
      runMain();
      break;

    case State::Debug:
      runDebug();
      break;
  }
}
