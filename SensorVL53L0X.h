#ifndef SENSORVL53L0X_H
#define SENSORVL53L0X_H

#include <stdint.h>

#include "VL53L0X.h"

struct SensorVL53L0XConfig {
  private:
    static uint8_t toAngle(long degrees);

  public:
    uint8_t address = 0;
    uint8_t angle = 0;
    SensorVL53L0XConfig();
    SensorVL53L0XConfig(uint8_t address, long degrees);
    void clear();
    long getAngle() const;
    void setAngle(long degrees);
};

struct SensorMeasure {
  unsigned long timestamp = 0;
  unsigned int distance = 0;
};

class SensorVL53L0X {
  private:
    SensorVL53L0XConfig config;
    SensorMeasure measure;
    bool enabled;

  public:
    SensorVL53L0X();
    void clearConfiguration();
    const SensorVL53L0XConfig& getConfig() const;
    const SensorMeasure& getMeasure() const;
    bool isConfigured() const;
    bool isEnabled() const;
    void setConfig(SensorVL53L0XConfig &config);
    void setDistance(unsigned int value);
    void setEnabled(bool value);
};

#endif
